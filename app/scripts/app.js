'use strict';

/**
 * @ngdoc overview
 * @name evaWebappApp
 * @description
 * # evaWebappApp
 *
 * Main module of the application.
 */
angular
    .module('evaWebappApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch'
    ])
    .config(function($routeProvider) {
        $routeProvider
        /* .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })*/
        .when('/about', {
            templateUrl: 'views/about.html',
            controller: 'AboutCtrl',
            controllerAs: 'about'
        })
            .when('/', {
                templateUrl: 'views/login.html',
                controller: 'loginCtrl',
                controllerAs: 'login'
            })
            .when('/dashboard', {
                templateUrl: 'views/dashboard.html',
                controller: 'dashboardCtrl',
                controllerAs: 'dashboard'
            })
            .otherwise({
                redirectTo: '/404.html'
            });
    });