'use strict';

/**
 * @ngdoc function
 * @name evaWebappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the evaWebappApp
 */
angular.module('evaWebappApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
