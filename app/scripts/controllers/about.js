'use strict';

/**
 * @ngdoc function
 * @name evaWebappApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the evaWebappApp
 */
angular.module('evaWebappApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
